from random import randint

class MyShip(object):

    def __init__(self, name, hits_needed, marker):

        self.name = name                # name of ship
        self.hits_needed = hits_needed  # hits needed to sink ship
        self.marker = marker            # reference point to place ship

    def is_start_pt(self, start_pt, orientation, board):

        if orientation == "horizontal":
            for point in range(self.hits_needed):
                if board[start_pt[0]][start_pt[1] + point] != "-":
                    return False

        if orientation == "vertical":
            for point in range(self.hits_needed):
                if board[start_pt[0] + point][start_pt[1]] != "-":
                    return False

        return True

    def make_ship(self, board):
        
        possible_horizontal = []
        possible_vertical = []
        self.max_to_edge = 10 - self.hits_needed
        
        for r in range(10):
            for c in range(self.max_to_edge + 1):
                possible_horizontal.append([r, c])

        for c in range(10):
            for r in range(self.max_to_edge + 1):
                possible_vertical.append([r, c])

        orientation = randint(1, 2)

        # horizontal placement
        if orientation == 1:
            list_pick = randint(0, len(possible_horizontal))
            start_coords_ls = possible_horizontal[list_pick - 1]

            while not self.is_start_pt(start_coords_ls, "horizontal", board):
                list_pick = randint(0, len(possible_horizontal))
                start_coords_ls = possible_horizontal[list_pick - 1]
            for c in range(self.hits_needed):
                board[start_coords_ls[0]][start_coords_ls[1] + c] = self.marker

        # vertical placement
        if orientation == 2:
            list_pick = randint(0, len(possible_vertical))
            start_coords_ls = possible_vertical[list_pick - 1]

            while not self.is_start_pt(start_coords_ls, "vertical", board):
                list_pick = randint(0, len(possible_vertical))
                start_coords_ls = possible_vertical[list_pick - 1]
            for c in range(self.hits_needed):
                board[start_coords_ls[0] + c][start_coords_ls[1]] = self.marker

    def hit_ship(self):
        self.hits_needed -= 1
        if self.hits_needed == 0:
            return "*-.-*-.-Your opponent sunk your %s!-.-*-.-*" % self.name
        else:
            return "*-.-Your opponent hit your %s!-.-*" % self.name

class EnemyShip(MyShip):

    def hit_ship(self):
        self.hits_needed -= 1
        if self.hits_needed == 0:
            return "*-.-*-.-You sunk the %s!-.-*-.-*" % self.name
        else:
            return "*-.-You hit the %s! Hit it %i more times to sink!-.-*" % (
                                                    self.name, self.hits_needed)

class Board(object):
    
    def __init__(self):
        self.board = []
        for x in range(10):
            self.board.append(["-"] * 10)
        self.row_names = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']

    def print_board(self):
        print("   _1_2_3_4_5_6_7_8_9_10")

        print(self.row_names[0], "|", " ".join(self.board[0]), "Total hits")
        print(self.row_names[1], "|", " ".join(self.board[1]), "left:")
        print(self.row_names[2], "|", " ".join(self.board[2]), total_my_hits_left)
        print(self.row_names[3], "|", " ".join(self.board[3]))
        print(self.row_names[4], "|", " ".join(self.board[4]))
        print(self.row_names[5], "|", " ".join(self.board[5]))
        print(self.row_names[6], "|", " ".join(self.board[6]))
        print(self.row_names[7], "|", " ".join(self.board[7]))
        print(self.row_names[8], "|", " ".join(self.board[8]))
        print(self.row_names[9], "|", " ".join(self.board[9]))
        print("\n")

class Grid(Board):

    def __init__(self):
        self.board = []
        for x in range(10):
            self.board.append(["-"] * 10)
        self.row_names = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']

    def print_board(self):
        print("   _1_2_3_4_5_6_7_8_9_10")
        print(self.row_names[0], "|", " ".join(self.board[0]), "Total hits")
        print(self.row_names[1], "|", " ".join(self.board[1]), "left:")
        print(self.row_names[2], "|", " ".join(self.board[2]), total_enemy_hits_left)
        print(self.row_names[3], "|", " ".join(self.board[3]))
        print(self.row_names[4], "|", " ".join(self.board[4]))
        print(self.row_names[5], "|", " ".join(self.board[5]))
        print(self.row_names[6], "|", " ".join(self.board[6]), "   O = miss")
        print(self.row_names[7], "|", " ".join(self.board[7]), "   X = HIT!")
        print(self.row_names[8], "|", " ".join(self.board[8]))
        print(self.row_names[9], "|", " ".join(self.board[9]))

def my_turn():

        guess = ""

        while guess == "":
            guess = input("Input shooting coordinates: ")
        
        row = guess[0].upper()
        col = guess[1:]
        
        while (row not in ['A','B','C','D','E','F','G','H','I','J'] or
               int(col) not in list(range(1, 11))
        ):
            print("That's not a valid coordinate!")
            guess = input("Try again: ")

            row = guess[0].upper()
            col = guess[1:]

        print("\n" * 40)

        result = my_shot_result(row, col)
        print_player_view(result)
    
def my_shot_result(guess_row, guess_col):

    row_dict = {'A':0, 'B':1, 'C':2, 'D':3, 'E':4,
                'F':5, 'G':6, 'H':7, 'I':8, 'J':9}
    col_dict = {'1':0, '2':1, '3':2, '4':3, '5':4,
                '6':5, '7':6, '8':7, '9':8, '10':9}

    guess_row = row_dict[guess_row]
    guess_col = col_dict[guess_col]
    shot_location = enemy_board.board[guess_row][guess_col]

    if (attack_grid.board[guess_row][guess_col] == 'X' or
        attack_grid.board[guess_row][guess_col] == 'O'):
        return "You've already tried shooting there, ya idiot!"
    elif shot_location == '-':
        attack_grid.board[guess_row][guess_col] = "O"
        return "*-.-*-.-You missed-.-*-.-*!"
    elif shot_location == 'C':
        attack_grid.board[guess_row][guess_col] = 'X'
        return enemy_carrier.hit_ship()
    elif shot_location == 'B':
        attack_grid.board[guess_row][guess_col] = 'X'
        return enemy_battleship.hit_ship()
    elif shot_location == 'S':
        attack_grid.board[guess_row][guess_col] = 'X'
        return enemy_submarine.hit_ship()
    elif shot_location == 'D':
        attack_grid.board[guess_row][guess_col] = 'X'
        return enemy_destroyer.hit_ship()
    elif shot_location == 'P':
        attack_grid.board[guess_row][guess_col] = 'X'
        return enemy_patrol.hit_ship()

class Enemy(object):

    def __init__(self):
        self.possible_shots = []
        for r in range(10):
            for c in range(10):
                self.possible_shots.append([r, c])

    def enemy_turn(self):
        print("\n" * 40)
        
        random = randint(0, len(self.possible_shots) - 1)
        result = self.possible_shots[random]
        
        while (my_board.board[result[0]][result[1]] == 'X' or
               my_board.board[result[0]][result[1]] == 'O'):
            random = randint(0, len(self.possible_shots) - 1)
            result = self.possible_shots[random]
        
        result = self.enemy_shot_result(result[0], result[1])

        print_player_view(result)

    def enemy_shot_result(self, guess_row, guess_col):
        shot_location = my_board.board[guess_row][guess_col]

        if shot_location == '-':
            my_board.board[guess_row][guess_col] = "O"
            return "*-.-*-.-Your opponent missed!-.-*-.-*"
        elif shot_location == 'C':
            my_board.board[guess_row][guess_col] = 'X'
            return my_carrier.hit_ship()
        elif shot_location == 'B':
            my_board.board[guess_row][guess_col] = 'X'
            return my_battleship.hit_ship()
        elif shot_location == 'S':
            my_board.board[guess_row][guess_col] = 'X'
            return my_submarine.hit_ship()
        elif shot_location == 'D':
            my_board.board[guess_row][guess_col] = 'X'
            return my_destroyer.hit_ship()
        elif shot_location == 'P':
            my_board.board[guess_row][guess_col] = 'X'
            return my_patrol.hit_ship()
        
my_board = Board()
enemy_board = Board()
attack_grid = Grid()

enemy = Enemy()

# Make user's ships:
my_carrier = MyShip("carrier", 5, "C")
my_battleship = MyShip("battleship", 4, "B")
my_submarine = MyShip("submarine", 3, "S")
my_destroyer = MyShip("destroyer", 3, "D")
my_patrol = MyShip("patrol boat", 2, "P")

# Make CPU's ships:
enemy_carrier = EnemyShip("carrier", 5, "C")
enemy_battleship = EnemyShip("battleship", 4, "B")
enemy_submarine = EnemyShip("submarine", 3, "S")
enemy_destroyer = EnemyShip("destroyer", 3, "D")
enemy_patrol = EnemyShip("patrol boat", 2, "P")

# Set user's board:
my_carrier.make_ship(my_board.board)
my_battleship.make_ship(my_board.board)
my_submarine.make_ship(my_board.board)
my_destroyer.make_ship(my_board.board)
my_patrol.make_ship(my_board.board)

# Set CPU's board:
enemy_carrier.make_ship(enemy_board.board)
enemy_battleship.make_ship(enemy_board.board)
enemy_submarine.make_ship(enemy_board.board)
enemy_destroyer.make_ship(enemy_board.board)
enemy_patrol.make_ship(enemy_board.board)

def print_player_view(message):
    attack_grid.print_board()
    print("\n", message, "\n")
    my_board.print_board()

total_enemy_hits_left = (enemy_carrier.hits_needed +
                         enemy_battleship.hits_needed +
                         enemy_submarine.hits_needed +
                         enemy_destroyer.hits_needed +
                         enemy_patrol.hits_needed)

total_my_hits_left = (my_carrier.hits_needed +
                      my_battleship.hits_needed +
                      my_submarine.hits_needed +
                      my_destroyer.hits_needed +
                      my_patrol.hits_needed)

print_player_view("*-.-*-Let's play Battleship!-*-.-*")

total_enemy_hits_left = 1
total_my_hits_left = 1

while total_enemy_hits_left > 0 and total_my_hits_left > 0:
    
    total_enemy_hits_left = (enemy_carrier.hits_needed +
                             enemy_battleship.hits_needed +
                             enemy_submarine.hits_needed +
                             enemy_destroyer.hits_needed +
                             enemy_patrol.hits_needed)

    total_my_hits_left = (my_carrier.hits_needed +
                          my_battleship.hits_needed +
                          my_submarine.hits_needed +
                          my_destroyer.hits_needed +
                          my_patrol.hits_needed)

    my_turn()
    input("Hit ENTER for opponent's turn.")
    enemy.enemy_turn()

if total_enemy_hits_left == 0:
    print("You win!")

if total_my_hits_left == 0:
    print("You lose!")